extends Area2D

var speed = -1
var shootable = true
var anim = "Bite"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$AnimatedSprite.play(anim)
	position.x += speed
	
	if position.x < -100:
		self.queue_free()
	
func spawn(pos_):
	position = pos_
	
func death_animation():
	anim = "Death"
	$death_clock.start()

func _on_Duk_Enemy_area_entered(area):
	if not area.shootable:
		death_animation()

func _on_death_clock_timeout():
	queue_free()
