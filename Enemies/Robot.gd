extends Area2D

var _Bullet = preload("res://Enemies/BotBullet.tscn")

var speed = -1
var shootable = true
var anim = "Bot"

# Called when the node enters the scene tree for the first time.
func _ready():
	$shoot_clock.start()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$AnimatedSprite.play(anim)
	position.y += speed
	
	if position.y < -100:
		self.queue_free()
	
func spawn(pos_):
	position = pos_

func death_animation():
	anim = "Death"
	$death_clock.start()

func _on_Robot_area_entered(area):
	if not area.shootable:
		death_animation()

func _on_death_clock_timeout():
	queue_free()

func _on_shoot_clock_timeout():
	var bullet = _Bullet.instance()
	add_child(bullet)
	$shoot_clock.start()
