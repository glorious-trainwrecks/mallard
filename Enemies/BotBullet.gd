extends Area2D

var speedx = -500
var speedy = 1
var shootable = true

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x += speedx * delta
	position.y += speedy
	
	if position.x < -1000:
		self.queue_free()
	
func spawn(pos_):
	position = pos_
	
func _on_BotBullet_area_entered(area):
	if not area.shootable:
		queue_free()