extends Area2D

var speed = -1
var shootable = true
var anim = "Wobble"
var YMAX
var SAFETY = 25
var dir = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	YMAX = get_viewport_rect().size.y

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	$AnimatedSprite.play(anim)
	position.x += speed
	position.y += dir * speed
	
	if position.x < -100:
		self.queue_free()
	if position.y < SAFETY or position.y > YMAX-SAFETY:
		dir *= -1

func spawn(pos_):
	position = pos_
	
func death_animation():
	anim = "Death"
	$death_clock.start()
	
func _on_Squid_area_entered(area):
	if not area.shootable:
		death_animation()

func _on_death_clock_timeout():
	queue_free()