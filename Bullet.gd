extends Area2D

var velocity = 500
var screen_size
var can_shoot
var shootable = false

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size.x
	can_shoot = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position.x += velocity * delta
	if (position.x > screen_size):
		can_shoot = true
		
func spawn(object):
	position.x = object.nozzle.x
	position.y = object.nozzle.y
	can_shoot = false
	
func despawn():
	# Just hide it offscreen, this is a trainwreck after all.
	position.x = -999
	position.y = -999
	can_shoot = true

func _on_Bullet_area_entered(area):
	if area.shootable:
		despawn()
