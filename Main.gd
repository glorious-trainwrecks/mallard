extends Node2D

var _Bullet
var _DukEnemy = preload("res://Enemies/Duk_Enemy.tscn")
var _Squid = preload("res://Enemies/Squid.tscn")
var _Bot = preload("res://Enemies/Robot.tscn")

var isStarted = false
var isOver = false
var score

var spawn_points = [
	Vector2(1200, 100),
	Vector2(1200, 250),
	Vector2(1200, 400),
	Vector2(1200, 550)
]
var bot_points = [
	Vector2(300, 900),
	Vector2(500, 900),
	Vector2(700, 900),
	Vector2(900, 900)
]

var NSPAWNS = 4

# Called when the node enters the scene tree for the first time.
func _ready():
	_Bullet = null
	score = 0
	
	$Player.position = Vector2(50, 300)
	
func _start():
	$MobTimer.start()
	$BotTimer.start()
	$ScoreTimer.start()
	$StartScreen.queue_free()
	isStarted = true

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	# Shoot bullet
	if Input.is_action_pressed("ui_accept") and isStarted:
		if _Bullet == null:
			_Bullet = preload("res://Bullet.tscn")
			add_child(_Bullet.instance())
		var bullet = get_node("Bullet")
		if bullet.can_shoot and isStarted:
			bullet.spawn($Player)
			
	if !$Player.alive:
		game_over()
	
	if not isStarted and Input.is_action_just_pressed("ui_accept"):
		_start() # Starts all the timers

func spawnEnemy(type_, index):
	var enemy = type_.instance()
	enemy.spawn(spawn_points[index])
	add_child(enemy)
	
func spawnBot(index):
	var bot = _Bot.instance()
	bot.spawn(bot_points[index])
	add_child(bot)

func _on_MobTimer_timeout():
	$MobTimer.start()
	var index = randi() % NSPAWNS
	var mondex = randi() % 4
	
	if mondex == 0:
		spawnEnemy(_Squid, index)
	else:
		spawnEnemy(_DukEnemy, index)

func _on_BotTimer_timeout():
	$BotTimer.start()
	var index = randi() % NSPAWNS
	spawnBot(index)

func game_over():
	if not isOver:
		$GameOverTimeout.start()
		$MobTimer.stop()
		$BotTimer.stop()
		$ScoreTimer.stop()
	isOver = true

func _on_GameOverTimeout_timeout():
	for node in get_children():
		if node.get("shootable"):
			remove_child(node)
	$GameOver.init()

func _on_ScoreTimer_timeout():
	score += 1
	$CanvasLayer.get_node("Label").text = str(score)
	$ScoreTimer.start()
