extends Node2D

var screen_width
var speed = 150

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_width = get_viewport_rect().size.x

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	position -= delta * Vector2(speed, 0)
	if position.x < 0:
		position.x += screen_width