extends Area2D

export var speed = 400
var screen_size
var bullet
var nozzle = Vector2()
var shootable = false
var alive = true
var anim = "DuckWalk"

# Called when the node enters the scene tree for the first time.
func _ready():
	screen_size = get_viewport_rect().size

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var velocity = Vector2()  # The player's movement vector.
	if Input.is_action_pressed("ui_right"):
		velocity.x += 1
	if Input.is_action_pressed("ui_left"):
		velocity.x -= 1
	if Input.is_action_pressed("ui_down"):
		velocity.y += 1
	if Input.is_action_pressed("ui_up"):
		velocity.y -= 1
	if velocity.length() > 0:
		velocity = velocity.normalized() * speed

	$AnimatedSprite.play(anim)

	position += velocity * delta
	position.x = clamp(position.x, 0, screen_size.x)
	position.y = clamp(position.y, 0, screen_size.y)
	nozzle = Vector2(position.x + 40, position.y - 20)

func _on_Player_area_entered(area):
	if area.shootable:
		death_animation()
		alive = false
		$death_clock.start()

func death_animation():
	anim = "Death"

func _on_death_clock_timeout():
	hide()
